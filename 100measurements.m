#!/usr/bin/env octave

global fname;
% file name
fname = 'recording.wav';

% the amount of seconds of no sound at the start of recording
init_blank_secs = 2;
% The same but for the end of recording
end_blank_secs = 2;

%% Thanks to https://www.mathworks.com/matlabcentral/answers/281261-frequency-domain-analysis-of-a-audio-signal
function max_freq = get_max_freq(samples_range)
	% bring it to scope
	global fname;
	try
		[audio_in ,audio_freq_sampl] = audioread(fname, samples_range);
	catch ME
		disp(sprintf("failed to use samples_range", samples_range(1)));
	end
	Length_audio = length(audio_in);
	df = audio_freq_sampl/Length_audio;
	frequency_audio = - audio_freq_sampl/2:df:audio_freq_sampl/2-df;
	FFT_audio_in = fftshift(fft(audio_in))/length(fft(audio_in));
	% figure
	% plot(frequency_audio, abs(FFT_audio_in));
	% title('FFT of Input Audio');
	% xlabel('Frequency(Hz)');
	% ylabel('Amplitude');
	[m, i] = max(abs(FFT_audio_in));
	max_freq = abs(frequency_audio(i));
end

% calculate trivial info
finfo = audioinfo(fname);
total_samples = finfo.TotalSamples
sample_rate = finfo.SampleRate
total_secs = total_samples/sample_rate

% trivial info considering constants
init_blank_samps = init_blank_secs * sample_rate
end_blank_samps = end_blank_secs * sample_rate;
% total number of notes played, considering the duration of each play was of 1 [s]
tot_plays = total_secs - end_blank_secs - init_blank_secs

% The amount of samples in which notes were played
real_samps = total_samples - (init_blank_samps + end_blank_samps);
one_sample_len = real_samps / tot_plays
% for sanity check
one_sample_time = one_sample_len/sample_rate
if one_sample_len < 0
	error("error: one_sample_len is negative, it means the constants are not set right")
	return
end

frequencies = zeros(round(tot_plays),1);
% Given the solution for the equation (for k):
%
%   440*(2^k) = max_freq
%
% per frequency, we measure how far k is from an integer
distances = zeros(round(tot_plays),1);
for i = 1:tot_plays
	start_samp = init_blank_samps + one_sample_len * (i - 1);
	end_samp = init_blank_samps + one_sample_len * i;
	max_freq = get_max_freq(round([start_samp, end_samp]));
	log_val = log2(max_freq/440);
	distances(i) = round(log_val) - log_val;
	frequencies(i) = max_freq;
end

sigma = std(distances)

mue = mean(distances)

% A bit heuristic nbits argument - it should be relative to tot_plays but 3
% there is a bit arbitrary.
[nn, xx] = hist(distances, round(tot_plays/6))

% We want to make std_func has a maxima around x = 0. Hence we calculate
% 'factor' in such a way that it'll be a solution to the equation:
%
%   factor * original_std_func(x=0) = max(nn)
%
% where max(nn) is the maximum value of the y axis in the plot.
%
factor = max(nn) * (sqrt(2*pi * sigma^2));
std_func = @(x) (factor / sqrt(2*pi * sigma^2)) * exp(-((x - mue)^2)/(2 * sigma^2))
bar(xx, nn)
hold on;
fplot(std_func, [-0.5, 0.5])

xticks_arr_right = (mue+sigma):sigma:1;
xticks_arr_left = -1:sigma:mue;
xticks_arr = cat(2, xticks_arr_left, xticks_arr_right);
xticks(xticks_arr);
xticks_len = length(xticks_arr);
xticklabels_arr = cell(xticks_len)
for i=1:xticks_len/2
	xticklabels_arr{i} = sprintf('%d', i - xticks_len/2 -1);
	xticklabels_arr{i + xticks_len/2} = sprintf('%d', i -1);
end
xticklabels(xticklabels_arr)

ylabel("Frequencies of k's distances from an integer");
xlabel('Distance of k from an integer, in terms of \sigma');
legend({ ...
		"Measured",...
		"Fitted histogram plot", ...
	}, ...
	'Location', "northeast" ...
)

print -dpng "final-log-plot.png"
