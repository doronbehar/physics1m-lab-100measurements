100measurements.pdf: 100measurements.tex final-log-plot.png
	tectonic 100measurements.tex

final-log-plot.png: 100measurements.m
	octave 100measurements.m

all: 100measurements.pdf

open: 100measurements.pdf
	gio open 100measurements.pdf

.PHONY: open
